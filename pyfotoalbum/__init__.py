import http.server
import os
from pathlib import Path
import socketserver
import subprocess
import time
from urllib.parse import unquote

import click


IMAGE_EXTENSIONS = {"jpg", "jpeg", "png", "gif", "webp", "ico"}


FIXED_FILES = {
    "favicon.ico",
    "unknown.png",
    "thumbnail-unavailable.png",
}


DIRECTORY = None
CACHE = None


def get_thumbnail(path, regenerate=False):
    source = DIRECTORY / path
    cache = CACHE / path
    if not cache.is_file() or regenerate:
        if len(cache.parts) > 1:
            # make sure subdirs exist
            Path(CACHE / Path(*cache.parts[:-1])).mkdir(
                mode=511, parents=True, exist_ok=True
            )
        start = time.monotonic()
        exitcode = subprocess.run(
            ["convert", source, "-scale", "250", cache]
        ).returncode
        print(f"thumbnail took {time.monotonic()-start:.3}s: {cache}")
        if exitcode != 0:
            print(f"ERROR: could not generate thumbnail: {cache} -> {exitcode}")
            with open("thumbnail-unavailable.png", "rb") as f:
                return f.read()
    with open(cache, "rb") as f:
        return f.read()


def get_mimetype(ext):
    if ext in IMAGE_EXTENSIONS:
        return f"image/{ext}"
    else:
        return "data/binary"


def get_file_content(path):
    if path.endswith("~thumb"):
        path = path[:-6]  # lose the thumb
        is_thumb = True
    else:
        is_thumb = False
    ext = path.rsplit(".", maxsplit=1)[-1].lower()
    if is_thumb:
        return get_thumbnail(path), get_mimetype(ext)
    if path in FIXED_FILES:
        with open(path, "rb") as f:
            return f.read(), get_mimetype(ext)
    else:
        with open(DIRECTORY / path, "rb") as f:
            return f.read(), get_mimetype(ext)


def get_entry(path, entry):
    if path and not path.startswith("/"):
        path = f"/{path}"
    ext = entry.rsplit(".", maxsplit=1)[-1].lower()
    if ext in IMAGE_EXTENSIONS:
        return f'<a href="{path}/{entry}"><img src="{path}/{entry}~thumb" /></a> | '
    else:
        return f'<a href="{path}/{entry}"><img src="/unknown.png" />{entry}</a> | '


def get_directory(path):
    result = []
    for entry in sorted(os.listdir(DIRECTORY / path)):
        print(f"entry: {path}/{entry}")
        result.append(get_entry(path, entry))
    return (
        f"<html>\n<head>\n<title>{path}</title>\n</head>\n<body>\n"
        + "\n".join(result)
        + "\n</body>\n</html>"
    )


class Fotoalbum(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        path = unquote(self.path).strip("/")
        Path(DIRECTORY).joinpath(path).resolve().relative_to(
            DIRECTORY.resolve()
        )  # catches path traversal
        try:
            content, content_type = get_file_content(path)
            self.send_response(200, "Fotoalbum")
            self.send_header("Content-type", content_type)
            self.end_headers()
            self.wfile.write(content)
        except IsADirectoryError:
            self.send_response(200, "Fotoalbum")
            self.send_header("Content-type", "text/html; charset=utf-8")
            self.end_headers()
            self.wfile.write(get_directory(path).encode("utf-8"))


@click.command()
@click.option("--port", default=8000)
@click.option("--directory", help="http root directory", required=True)
@click.option("--cache", help="thumbnail cache dir", default="/tmp/fotoalbum-cache")
def cli(port, directory, cache):
    global DIRECTORY, CACHE
    DIRECTORY = Path(directory)
    CACHE = Path(cache)
    with socketserver.TCPServer(("", port), Fotoalbum) as httpd:
        print("HTTP Server listening on port", port)
        httpd.serve_forever()


if __name__ == "__main__":
    cli()
