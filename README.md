# Fotoalbum

HTTP server with directory listing suitable for photos.

    sudo apt install imagemagick

    python pyfotoalbum/__init__.py --port 8000 --directory /storage/photos --cache /tmp/fotoalbum

